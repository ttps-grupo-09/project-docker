# project-docker
La aplicación se encuentra "Dockerizada", lo que quiere decir que cada repositorio contará con su propio contenedor, facilitando el posterior Dev-Ops.

## Software necesario para el ambiente de desarrollo

Para preparar el ambiente de desarrollo, es necesario primero contar con las siguientes dependencias instaladas en nuestro sistema:
* Docker.
* Docker-compose.
* Cliente Git en terminal.

Observación: se recomienda utilizar Linux (cualquier distro) para el correcto setup del proyecto. 

## Setup del ambiente de desarrollo

Para levantar el proyecto en local, se deben seguir los siguiente pasos:

1. Tener instaladas todas las dependencias anteriormente requeridas.
1. Preparar un directorio en donde se instanciaran los repositorios. Por ejemplo: **mkdir proyecto** seguido de **cd proyecto**
1. Clonar el repositorio **project-docker** (actual). Para ello, dicho clone puede realizarse por medio del protocolo HTTPS o bien podria hacerse usando una clave SSH: **git clone git@gitlab.com:ttps-grupo-09/project-docker.git**
1. Moverse al nuevo repo con **cd project-docker**
1. Clonar el repositorio [**project**](https://gitlab.com/ttps-grupo-09/project): **git clone git@gitlab.com:ttps-grupo-09/project.git**
1. Buildear imagen del proyecto: **docker-compose -f docker-compose.dev.yml up --build**
1. Luego del building, acceder a http://localhost:8000 y corroborar que la aplicación se encuentra funcional.

## Consideraciones en general

* Luego de haber buildeado por primera vez, se debe siempre levantar el proyecto con **docker-compose -f docker-compose.dev.yml up -d**
* Para detener el servidor local: **docker-compose -f docker-compose.dev.yml down**
* Para utilizar python manage.py [command] dentro del contenedor por medio de nuestra terminal debemos usar el siguiente script: source execute [command].
* Cada vez que se llama al **. execute startapp <nombre_app>** para la creación de una nueva Django App, Docker crea dicho directorio con el root user del mismo Docker. Usar **sudo chown -R $USER project/nombre_app/**

