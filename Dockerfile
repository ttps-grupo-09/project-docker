FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD ./project/requirements.txt /code/
RUN pip install -r /code/requirements.txt
ADD ./project /code
